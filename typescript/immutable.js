// strings, numbers, booleans - primitive data types - immutable

let a = 5;
let b = a; // copying the value from a to b
b = 8;
console.log(a, b);

// arrays, objects - reference data types - mutable
let x = [1, 2, 3];
let z = [5, 6, 7];
//let y = x; // copying the address of x
let y = [...x, 4, ...z]; // copying values of x with spread operator
//y.push(4);
console.log(x, y);

let p = { name: "krishna", age: 33 };
let q = { ...p };
q.age = 56;
console.log(p, q);
