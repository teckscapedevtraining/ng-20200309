// es5

// function greet() {
//   console.log("Hello world");
// }

// const greet = function() {
//   console.log("Hello world");
// };

// es6

const greet = (name, age) => {
  age += 10;
  console.log(`Hello ${name} who is ${age} years old`);
};
// arrow / fat arrow function with template literals

greet("krishna", 34);
