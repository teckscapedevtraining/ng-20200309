// SYNCHRONOUS PROGRAMMING
// const addNumbers = (a, b) => {
//   squareNumbers(a, b);
//   console.log("result printed..");
// };

// const squareNumbers = (x, y) => {
//   x = x * x;
//   y = y * y;
//   console.log(x + y);
// };

// addNumbers(9, 45);

// ASYNC PROGRAMMING

const addNumbers = (a, b, callback) => {
  callback(a, b);
  console.log("result printed..");
};

const squareNumbers = (x, y) => {
  x = x * x;
  y = y * y;
  setTimeout(() => console.log(x + y), 1000);
  //   console.log(x + y);
};

addNumbers(9, 45, squareNumbers);
