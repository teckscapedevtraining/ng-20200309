const numbers = [1, 2, 3, 4, 5, 6, 7];
// numbers.forEach(number => {
//   const square = number * number;
//   console.log("The square of " + number + " is " + square);
// });

numbers.forEach(function(number) {
  const square = number * number;
  console.log("The square of " + number + " is " + square);
});

// numbers.forEach(number => {
//   if (number % 2 == 0) {
//     console.log(number);
//   }
// });

// sum = 0;
// numbers.forEach(number => {
//   sum += number;
// });
// console.log(sum);

const evens = numbers.filter(number => number % 2 == 0);
console.log(evens);

const result = numbers.reduce((sum, number) => sum + number, 0);
console.log(result);
