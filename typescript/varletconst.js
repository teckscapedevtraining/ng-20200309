function printName() {
  var name = "krishna"; // function scoped
  const email = "krishna@example.com"; // block scoped
  var age = 45;
  {
    let age = 34; // block scoped
    const email = "krishna2@example.com";
    console.log(name, age, email);
  } // let const

  console.log(name, age, email);
}

printName();
