function greet() {
  const name = "john";
  const user = {
    name: document.getElementById("name").value,
    age: document.getElementById("age").value,
    email: "krishna@example.com",
    normalgreet: function() {
      console.log("hello " + this.name);
    },
    fagreet: () => console.log("hellow user")
  };

  console.log(user.normalgreet(), user.fagreet());
}
